# Map Fixes and Optimization

Map fixes and optimization

# nt_oilstain fixes

2019/11/11 - fix3  
Killed "dynamic" lights  

# nt_transit fixes

Optimization, bug fixing and other fixes for nt_transit

Version: fix2 rc1 (b18)

Aligned train doors with the trains.  
Added a detail to indicate the end of playable area in the train tracks.  
Added two tags to one corridor.  
Fixed ramp bugs that could kill players  
Fixed trains windows  
Small visual changes to one spawn.  
Fixed a bad cubemap transition  
Light shadows optimization  


Credits go to Studio Radi-8 for the original maps and assets.